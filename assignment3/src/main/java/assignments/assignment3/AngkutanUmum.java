package assignments.assignment3;

public class AngkutanUmum extends Benda {
    // TODO: Implementasikan abstract method yang terdapat pada class Benda

    @Override
    public void tambahPersentase() {
        persentaseMenular += 35;
    }

    @Override
    public String toString() {
        return String.format("ANGKUTAN UMUM %s", getNama());
    }

    public AngkutanUmum(String nama) {
        // TODO: Buat constructor untuk AngkutanUmum.
        // Hint: Akses constructor superclass-nya
        super(nama);
    }
}