package assignments.assignment3;

public class TombolLift extends Benda {
    // TODO: Implementasikan abstract method yang terdapat pada class Benda

    @Override
    public void tambahPersentase() {
        super.persentaseMenular += 20;
    }

    @Override
    public String toString() {
        return String.format("TOMBOL LIFT %s", getNama());
    }

    public TombolLift(String nama) {
        // TODO: Buat constructor untuk TombolLift.
        // Hint: Akses constructor superclass-nya
        super(nama);
    }
}