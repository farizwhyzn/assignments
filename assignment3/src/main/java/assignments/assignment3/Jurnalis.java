package assignments.assignment3;

public class Jurnalis extends Manusia {

    public Jurnalis(String nama) {
        // TODO: Buat constructor untuk Jurnalis.
        // Hint: Akses constructor superclass-nya
        super(nama);
    }

    @Override
    public String toString() {
        return String.format("JURNALIS %s", getNama());
    }

}