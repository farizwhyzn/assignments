package assignments.assignment3;

public class Positif implements Status {

    public String getStatus() {
        return "Positif";
    }

    public void tularkan(Carrier penular, Carrier tertular) {
        // TODO: Implementasikan apabila object Penular melakukan interaksi dengan object tertular
        // Hint: Handle kasus ketika keduanya benda dapat dilakukan disini
        //Tidak melakukan apapun ketika keduanya benda
        if (penular.getTipe().equalsIgnoreCase("Benda") &&
                tertular.getTipe().equalsIgnoreCase("Benda")) {
        }
        //Ubah status tertular menjadi positif
        else {
            tertular.ubahStatus("Positif");
        }
    }
}