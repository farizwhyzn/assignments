package assignments.assignment3;

public class PeganganTangga extends Benda {
    // TODO: Implementasikan abstract method yang terdapat pada class Benda

    @Override
    public void tambahPersentase() {
        persentaseMenular += 20;
    }

    @Override
    public String toString() {
        return String.format("PEGANGAN TANGGA %s", getNama());
    }

    public PeganganTangga(String nama) {
        // TODO: Buat constructor untuk Jurnalis.
        // Hint: Akses constructor superclass-nya
        super(nama);
    }

}