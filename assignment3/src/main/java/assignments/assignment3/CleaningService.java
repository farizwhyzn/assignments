package assignments.assignment3;

public class CleaningService extends Manusia {

    private int jumlahDibersihkan;

    public CleaningService(String nama) {
        // TODO: Buat constructor untuk CleaningService.
        // Hint: Akses constructor superclass-nya
        super(nama);
    }

    @Override
    public String toString() {
        return String.format("CLEANING SERVICE %s", getNama());
    }

    public void bersihkan(Benda benda) {
        // TODO: Implementasikan apabila objek CleaningService ini membersihkan benda
        // Hint: Update nilai atribut jumlahDibersihkan
        benda.ubahStatus("negatif");
        this.jumlahDibersihkan += 1;
        benda.setPersentaseMenular(0);
    }

    public int getJumlahDibersihkan() {
        // TODO: Kembalikan nilai dari atribut jumlahDibersihkan
        return this.jumlahDibersihkan;
    }

}