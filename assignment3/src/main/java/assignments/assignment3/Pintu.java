package assignments.assignment3;

public class Pintu extends Benda {
    // TODO: Implementasikan abstract method yang terdapat pada class Benda  

    @Override
    public void tambahPersentase() {
        super.persentaseMenular += 30;
    }

    public Pintu(String nama) {
        // TODO: Implementasikan apabila objek CleaningService ini membersihkan benda
        // Hint: Update nilai atribut jumlahDibersihkan
        super(nama);
    }

    @Override
    public String toString() {
        return String.format("PINTU %s", getNama());
    }
}