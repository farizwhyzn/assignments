package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public class World{

    public List<Carrier> listCarrier;

    public World(){
        // TODO: Buat constructor untuk class World
        listCarrier = new ArrayList<Carrier>();
    }

    public Carrier createObject(String tipe, String nama){
        // TODO: Implementasikan apabila ingin membuat object sesuai dengan parameter yang diberikan
        if (tipe.equalsIgnoreCase("Petugas_Medis")){
            return new PetugasMedis(nama);
        }
        else if (tipe.equalsIgnoreCase("Pekerja_Jasa")){
            return new PekerjaJasa(nama);
        }
        else if (tipe.equalsIgnoreCase("Jurnalis")){
            return new Jurnalis(nama);
        }
        else if (tipe.equalsIgnoreCase("Ojol")){
            return new Ojol(nama);
        }
        else if (tipe.equalsIgnoreCase("Cleaning_Service")){
            return new CleaningService(nama);
        }
        else if (tipe.equalsIgnoreCase("Pegangan_Tangga")){
            return new PeganganTangga(nama);
        }
        else if (tipe.equalsIgnoreCase("Pintu")){
            return new Pintu(nama);
        }
        else if (tipe.equalsIgnoreCase("Tombol_Lift")){
            return new TombolLift(nama);
        }
        else if (tipe.equalsIgnoreCase("Angkutan_Umum")){
            return new AngkutanUmum(nama);
        }
        else {
            return null;
        }
    }

    public Carrier getCarrier(String nama) {
        // TODO: Implementasikan apabila ingin mengambil object carrier dengan nama sesuai dengan parameter
        for (Carrier carrier : listCarrier) {
            if (carrier.getNama().equalsIgnoreCase(nama)) {
                return carrier;
            }
        }
        return null;
    }
}