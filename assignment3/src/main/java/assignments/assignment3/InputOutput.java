package assignments.assignment3;

import java.io.*;

public class InputOutput {

    private BufferedReader br;
    private PrintWriter pw;
    private String inputFile;
    private String outputFile;
    private World world;

    public InputOutput(String inputType, String inputFile, String outputType, String outputFile) throws FileNotFoundException {
        // TODO: Buat constructor untuk InputOutput.
        this.inputFile = inputFile;
        this.outputFile = outputFile;
        setBufferedReader(inputType);
        setPrintWriter(outputType);

    }

    public void setBufferedReader(String inputType) throws FileNotFoundException {
        // TODO: Membuat BufferedReader bergantung inputType (I/O text atau input terminal)
        try {
            if (inputType.equalsIgnoreCase("text")) {
                br = new BufferedReader(new FileReader(inputFile));
            } else if (inputType.equalsIgnoreCase("terminal")) {
                br = new BufferedReader(new InputStreamReader(System.in));
            }
        } catch (Exception e) {
            System.out.println("Error");
        }
    }

    public void setPrintWriter(String outputType) throws FileNotFoundException {
        // TODO: Membuat PrintWriter bergantung inputType (I/O text atau output terminal)
        try {
            if (outputType.equalsIgnoreCase("text")) {
                pw = new PrintWriter(new File(outputFile));
            } else if (outputType.equalsIgnoreCase("terminal")) {
                pw = new PrintWriter(System.out, true);
            }
        } catch (Exception e) {
            System.out.println("Error");
        }
    }

    public void run() throws IOException, BelumTertularException {
        // TODO: Program utama untuk InputOutput, jangan lupa handle untuk IOException
        // Hint: Buatlah objek dengan class World
        // Hint: Untuk membuat object Carrier baru dapat gunakan method CreateObject pada class World
        // Hint: Untuk mengambil object Carrier dengan nama yang sesua dapat gunakan method getCarrier pada class World
        World world = new World();
        while (true) {
            String input = br.readLine();
            String[] userInput = input.split(" ");
            String command = userInput[0];

            //Add
            if (command.equalsIgnoreCase("ADD")) {
                world.listCarrier.add(world.createObject(userInput[1], userInput[2]));
            }

            //Interaksi
            else if (command.equalsIgnoreCase("INTERAKSI")) {
                Carrier obj = world.getCarrier(userInput[1]);
                Carrier obj2 = world.getCarrier(userInput[2]);
                //Jika obj Negatif dan obj2 Positif
                if (obj.getStatusCovid().equalsIgnoreCase("Negatif")
                        && obj2.getStatusCovid().equalsIgnoreCase("Positif")) {
                    obj2.interaksi(obj);
                }
                //Jika obj Positif dan obj2 Negatif
                else if (obj.getStatusCovid().equalsIgnoreCase("Positif")
                        && obj2.getStatusCovid().equalsIgnoreCase("Negatif")) {
                    obj.interaksi(obj2);
                }
            }

            //Positifkan
            else if (command.equalsIgnoreCase("POSITIFKAN")) {
                world.getCarrier(userInput[1]).ubahStatus("Positif");
            }

            //Sembuhkan
            else if (command.equalsIgnoreCase("SEMBUHKAN")) {
                PetugasMedis petugasMedis = (PetugasMedis) world.getCarrier(userInput[1]);
                Manusia manusia = (Manusia) world.getCarrier(userInput[2]);

                petugasMedis.obati(manusia);
            }

            //Bersihkan
            else if (command.equalsIgnoreCase("BERSIHKAN")) {
                CleaningService cleaningService = (CleaningService) world.getCarrier(userInput[1]);
                Benda benda = (Benda) world.getCarrier(userInput[2]);

                cleaningService.bersihkan(benda);
            }

            //Rantai
            else if (command.equalsIgnoreCase("RANTAI")) {
                Carrier obj = world.getCarrier(userInput[1]);
                try {

                    //BelumTertularException
                    if (obj.getStatusCovid().equalsIgnoreCase("Negatif")) {
                        throw new BelumTertularException(obj.toString() + " berstatus negatif");
                    }
                    String output = "";
                    for (Carrier carrier : obj.getRantaiPenular()) {
                        if (obj.getRantaiPenular().size() - 1 == obj.getRantaiPenular().indexOf(carrier)) {
                            output += carrier.toString() + "\n";
                        } else {
                            output += carrier.toString() + " -> ";
                        }
                    }
                    pw.printf("Rantai penyebaran %s: %s", obj.toString(), output);
                } catch (BelumTertularException e) {
                    pw.println(e);
                }
            }

            //Kasus Total
            else if (command.equalsIgnoreCase("TOTAL_KASUS_DARI_OBJEK")) {
                Carrier obj = world.getCarrier(userInput[1]);
                pw.printf("%s telah menyebarkan virus COVID ke %d objek\n",
                        obj, obj.getTotalKasusDisebabkan());
            }

            //Kasus Aktif
            else if (command.equalsIgnoreCase("AKTIF_KASUS_DARI_OBJEK")) {
                Carrier obj = world.getCarrier(userInput[1]);
                pw.printf("%s telah menyebarkan virus COVID dan masih teridentifikasi positif" +
                        " sebanyak %d objek\n", obj, obj.getAktifKasusDisebabkan());
            }

            //Total Sembuh
            else if (command.equalsIgnoreCase("TOTAL_SEMBUH_MANUSIA")) {
                pw.printf("Total sembuh dari kasus COVID yang menimpa manusia ada sebanyak: %d kasus\n",
                        Manusia.getJumlahSembuh());
            }

            //Total Sembuh Medis
            else if (command.equalsIgnoreCase("TOTAL_SEMBUH_PETUGAS_MEDIS")) {
                PetugasMedis petugasMedis = (PetugasMedis) world.getCarrier(userInput[1]);
                pw.printf("%s menyembuhkan %d manusia\n", petugasMedis.toString(),
                        petugasMedis.getJumlahDisembuhkan());
            }

            //Total Bersih
            else if (command.equalsIgnoreCase("TOTAL_BERSIH_CLEANING_SERVICE")) {
                CleaningService cleaningService = (CleaningService) world.getCarrier(userInput[1]);
                pw.printf("%s membersihkan %d benda\n",
                        cleaningService.toString(), cleaningService.getJumlahDibersihkan());

            }
            //Keluar
            else if (command.equalsIgnoreCase("EXIT")) {
                br.close();
                pw.close();
                break;
            }
        }
    }
}
