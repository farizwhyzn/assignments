package assignments.assignment3;

public class PetugasMedis extends Manusia {

    private int jumlahDisembuhkan;

    public PetugasMedis(String nama) {
        // TODO: Buat constructor untuk Jurnalis.
        // Hint: Akses constructor superclass-nya
        super(nama);
    }

    @Override
    public String toString() {
        return String.format("PETUGAS MEDIS %s", getNama());
    }

    public void obati(Manusia manusia) {
        // TODO: Implementasikan apabila objek PetugasMedis ini menyembuhkan manusia
        // Hint: Update nilai atribut jumlahDisembuhkan
        //mengubah status manusia yang telah disembuhkan menjadi negatif
        super.tambahSembuh();
        jumlahDisembuhkan += 1;
        manusia.ubahStatus("Negatif");

        for (Carrier carrier : manusia.getRantaiPenular()) {
            if (carrier != manusia) {
                carrier.kurangAktifKasusDisebabkan();
            }
        }
        //Untuk menghilangkan double pada rantaiPenular
        manusia.getRantaiPenular().clear();
    }

    public int getJumlahDisembuhkan() {
        // TODO: Kembalikan nilai dari atribut jumlahDisembuhkan
        return jumlahDisembuhkan;
    }
}