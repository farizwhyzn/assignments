package assignments.assignment3;

public class Ojol extends Manusia {

    public Ojol(String nama) {
        // TODO: Buat constructor untuk Jurnalis.
        // Hint: Akses constructor superclass-nya
        super(nama);
    }

    @Override
    public String toString() {
        return String.format("OJOL %s", getNama());
    }

}