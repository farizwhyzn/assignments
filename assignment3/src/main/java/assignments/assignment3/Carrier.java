package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public abstract class Carrier {

    private String nama;
    private String tipe;
    private Status statusCovid;
    private int aktifKasusDisebabkan;
    private int totalKasusDisebabkan;
    private List<Carrier> rantaiPenular;

    public Carrier(String nama, String tipe) {
        // TODO: Buat constructor untuk Carrier.
        this.nama = nama;
        this.tipe = tipe;
        this.rantaiPenular = new ArrayList<Carrier>();
        this.statusCovid = new Negatif();
    }

    public String getNama() {
        // TODO : Kembalikan nilai dari atribut nama
        return this.nama;
    }

    public String getTipe() {
        // TODO : Kembalikan nilai dari atribut tipe
        return this.tipe;
    }

    public String getStatusCovid() {
        // TODO : Kembalikan nilai dari atribut statusCovid
        return this.statusCovid.getStatus();
    }

    public int getAktifKasusDisebabkan() {
        // TODO : Kembalikan nilai dari atribut aktifKasusDisebabkan
        return this.aktifKasusDisebabkan;
    }

    public void kurangAktifKasusDisebabkan() {
        // TODO : Kembalikan nilai dari atribut aktifKasusDisebabkan
        this.aktifKasusDisebabkan -= 1;
    }

    public int getTotalKasusDisebabkan() {
        // TODO : Kembalikan nilai dari atribut totalKasusDisebabkan
        return this.totalKasusDisebabkan;
    }

    public List<Carrier> getRantaiPenular() {
        // TODO : Kembalikan nilai dari atribut rantaiPenular
        return this.rantaiPenular;
    }

    public void ubahStatus(String status) {
        // TODO : Implementasikan fungsi ini untuk mengubah atribut dari statusCovid
        if (status.equalsIgnoreCase("positif")) {
            statusCovid = new Positif();
            rantaiPenular.add(this);
        } else if (status.equalsIgnoreCase("negatif")) {
            statusCovid = new Negatif();
        }
    }

    public void interaksi(Carrier lain) {
        // TODO : Objek ini berinteraksi dengan objek lain
        if (lain.getTipe().equalsIgnoreCase("Manusia")) {
            lain.rantaiPenular.addAll(this.rantaiPenular);
            for (Carrier carrier : this.rantaiPenular) {
                if (carrier != lain) {
                    carrier.totalKasusDisebabkan += 1;
                    carrier.aktifKasusDisebabkan += 1;
                }
            }
            this.statusCovid.tularkan(this, lain);
        } else if (lain.getTipe().equalsIgnoreCase("Benda")) {
            Benda benda = (Benda) lain;
            benda.tambahPersentase();
            if (benda.getPersentaseMenular() >= 100) {
                lain.rantaiPenular.addAll(this.rantaiPenular);
                for (Carrier carrier : this.rantaiPenular) {
                    if (carrier != lain) {
                        carrier.totalKasusDisebabkan += 1;
                        carrier.aktifKasusDisebabkan += 1;
                    }
                }
                this.statusCovid.tularkan(this, lain);
            }
        }
    }

    public abstract String toString();

}