package assignments.assignment1;

import java.util.ArrayList;
import java.util.Scanner;

public class HammingCode {

    static final int ENCODE_NUM = 1;
    static final int DECODE_NUM = 2;
    static final int EXIT_NUM = 3;

    private static boolean pangkat2(int n) {
        return Math.log(n) / Math.log(2) % 1 == 0;
    }

    /**
     * Encode.
     * mencari jumlah parity bit.
     * dan panjang total data.
     * menentukan letak-letak parity bit.
     * menentukan nilai dari parity bit.
     * menambah parity bit ke dalam arrayList hasil.
     * @param data data tom encode
     * @return encoded data
     */
    public static String encode(String data) {

        ArrayList<Integer> hasil = new ArrayList<>();
        int jumParity = 2;
        while (Math.pow(2, jumParity) - jumParity - 1 < data.length()) {
            jumParity++;
        }
        int panjangTotal = data.length() + jumParity;

        int counter = 0;
        for (int i = 1; i <= panjangTotal; i++) {

            if (pangkat2(i)) {
                hasil.add(-1);
            } else {
                hasil.add(Character.getNumericValue(data.charAt(counter++)));
            }
        }

        for (int i = 0; i < jumParity; i++) {
            int indexReal = (int) Math.pow(2, i);
            int parCounter = 0;
            for (int j = indexReal; j <= panjangTotal; j++) {
                if (j / indexReal % 2 == 1) {
                    if (hasil.get(j - 1) == 1) {
                        parCounter++;
                    }
                }
            }
            if (parCounter % 2 == 1) {
                hasil.set(indexReal - 1, 1);
            } else {
                hasil.set(indexReal - 1, 0);
            }
        }

        String hasilAkhir = "";
        for (int a : hasil) {
            hasilAkhir += a;
        }

        return hasilAkhir;
    }

    /**
     * Decode data.
     * menentukan jumlah parity bit.
     * menentukan posisi parity bit.
     * menghitung dan melakukan pengecekan parity.
     * replace jika ada parity yang salah.
     * dikembalikan ke data bit semula.
     * me-return data bit setelah dicek.
     * @param code code to check
     * @return decoded data
     */
    public static String decode(String code) {
        ArrayList<Integer> hasil = new ArrayList<>();

        for (int i = 0; i < code.length(); i++) {
            hasil.add(Character.getNumericValue(code.charAt(i)));
        }

        int jumParity = 2;
        while (Math.pow(2, jumParity) - 1 < code.length()) {
            jumParity++;
        }
        int panjangTotal = code.length();

        int errorCounter = 0;
        for (int i = 0; i < jumParity; i++) {
            int indexReal = (int) Math.pow(2, i);
            int parCounter = 0;
            for (int j = indexReal; j <= panjangTotal; j++) {
                if (j / indexReal % 2 == 1) {
                    if (hasil.get(j - 1) == 1) {
                        parCounter++;
                    }
                }
            }
            if (parCounter % 2 == 1) {
                errorCounter += indexReal;
            }
        }

        if (errorCounter > 0) {
            hasil.set(errorCounter - 1, 1 - hasil.get(errorCounter - 1));
        }

        String hasilAkhir = "";
        for (int i = 1; i <= panjangTotal; i++) {
            if (!pangkat2(i)) {
                hasilAkhir += hasil.get(i - 1);
            }
        }

        return hasilAkhir;
    }

    /**
     * Main program for Hamming Code.
     *
     * @param args unused
     */
    public static void main(String[] args) {
        System.out.println("Selamat datang di program Hamming Code!");
        System.out.println("=======================================");
        Scanner in = new Scanner(System.in);
        boolean hasChosenExit = false;

        while (!hasChosenExit) {
            System.out.println();
            System.out.println("Pilih operasi:");
            System.out.println("1. Encode");
            System.out.println("2. Decode");
            System.out.println("3. Exit");
            System.out.println("Masukkan nomor operasi yang diinginkan: ");
            int operation = in.nextInt();
            if (operation == ENCODE_NUM) {
                System.out.println("Masukkan data: ");
                String data = in.next();
                String code = encode(data);
                System.out.println("Code dari data tersebut adalah: " + code);
            } else if (operation == DECODE_NUM) {
                System.out.println("Masukkan code: ");
                String code = in.next();
                String data = decode(code);
                System.out.println("Data dari code tersebut adalah: " + data);
            } else if (operation == EXIT_NUM) {
                System.out.println("Sampai jumpa!");
                hasChosenExit = true;
            }
        }
        in.close();
    }
}
