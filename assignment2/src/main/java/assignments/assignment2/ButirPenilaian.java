package assignments.assignment2;

public class ButirPenilaian {

    private static final int PENALTI_KETERLAMBATAN = 20;
    private double nilai;
    private boolean terlambat;

    public ButirPenilaian(double nilai, boolean terlambat) {
        this.nilai = nilai;
        this.terlambat = terlambat;
        // TODO: buat constructor untuk ButirPenilaian.
    }

    public double getNilai() {
        // TODO: kembalikan nilai yang sudah disesuaikan dengan keterlambatan.
        if (this.nilai < 0) {
            return 0;
        } else {
            if (this.terlambat == true) {
                return this.nilai - PENALTI_KETERLAMBATAN / 100.0 * this.nilai;
            } else {
                return this.nilai;
            }

        }
    }

    @Override
    public String toString() {
        // TODO: kembalikan representasi String dari ButirPenilaian sesuai permintaan soal.
        if (this.terlambat == true) {
            return String.format("%.2f", getNilai()) + " (T)";
        } else {
            return String.format("%.2f", getNilai());
        }

    }
}
