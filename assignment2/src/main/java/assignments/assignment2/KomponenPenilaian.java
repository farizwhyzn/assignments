package assignments.assignment2;

public class KomponenPenilaian {

    private String nama;
    private ButirPenilaian[] butirPenilaian;
    private int bobot;

    public KomponenPenilaian(String nama, int banyakButirPenilaian, int bobot) {
        this.nama = nama;
        this.bobot = bobot;
        this.butirPenilaian = new ButirPenilaian[banyakButirPenilaian];

        // TODO: buat constructor untuk KomponenPenilaian. done
        // Note: banyakButirPenilaian digunakan untuk menentukan panjang butirPenilaian saja
        // (tanpa membuat objek-objek ButirPenilaian-nya).
    }

    /**
     * Membuat objek KomponenPenilaian baru berdasarkan bentuk KomponenPenilaian templat.
     *
     * @param templat templat KomponenPenilaian.
     */
    private KomponenPenilaian(KomponenPenilaian templat) {
        this(templat.nama, templat.butirPenilaian.length, templat.bobot);
    }

    /**
     * Mengembalikan salinan skema penilaian berdasarkan templat yang diberikan.
     *
     * @param templat templat skema penilaian sebagai sumber.
     * @return objek baru yang menyerupai templat.
     */
    public static KomponenPenilaian[] salinTemplat(KomponenPenilaian[] templat) {
        KomponenPenilaian[] salinan = new KomponenPenilaian[templat.length];
        for (int i = 0; i < salinan.length; i++) {
            salinan[i] = new KomponenPenilaian(templat[i]);
        }
        return salinan;
    }

    public void masukkanButirPenilaian(int idx, ButirPenilaian butir) {
        // TODO: masukkan butir ke butirPenilaian pada index ke-idx. done
        this.butirPenilaian[idx] = butir;

    }

    public String getNama() {
        // TODO: kembalikan nama KomponenPenilaian. done
        return this.nama;
    }

    public double getRerata() {
        // TODO: kembalikan rata-rata butirPenilaian.
        int counter = 0;
        for (int i = 0; i < this.butirPenilaian.length; i++) {
            if (this.butirPenilaian[i] != null) {
                counter += 1;
            }


        }
        double sum = 0;
        for (ButirPenilaian bp : butirPenilaian) {
            if (bp != null) {
                sum += bp.getNilai();
            }
        }
        if (counter == 0) {
            return 0;
        }

        return sum / counter;
    }

    public double getNilai() {
        // TODO: kembalikan rerata yang sudah dikalikan dengan bobot. done
        return getRerata() * this.bobot / 100;
    }

    public String getDetail() {
        // TODO: kembalikan detail KomponenPenilaian sesuai permintaan soal.
        int counter = 1;
        String header = String.format("~~~ %s (%d%%) ~~~\n", this.nama, this.bobot);
        if (butirPenilaian.length == 1) {
            for (int i = 0; i < butirPenilaian.length; i++) {
                if (butirPenilaian[i] != null) {
                    header += String.format("%s: %s\n", this.nama, "0.00");
                } else {
                    header += String.format("%s: %s\n", nama, butirPenilaian[i].toString());
                }
            }
            header += String.format("Kontribusi nilai akhir: %.2f", getNilai());
        } else {
            for (int i = 0; i < butirPenilaian.length; i++) {
                if (butirPenilaian[i] != null) {
                    header += String.format("%s %d: %s", this.nama,
                        counter++, butirPenilaian[i].toString());
                }

            }
            header += String.format("Rerata: %.2f", getRerata());
            header += String.format("Kontribusi nilai akhir: %.2f", getNilai());
        }

        return header;
    }

    @Override
    public String toString() {
        // TODO: kembalikan representasi String sebuah KomponenPenilaian sesuai permintaan soal.
        return String.format("Rerata %s: %.2f", this.nama, getRerata());
    }

}
