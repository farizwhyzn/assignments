package assignments.assignment2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AsistenDosen {

    private List<Mahasiswa> mahasiswa = new ArrayList<>();
    private String kode;
    private String nama;

    public AsistenDosen(String kode, String nama) {
        this.kode = kode;
        this.nama = nama;
        // TODO: buat constructor untuk AsistenDosen.
    }

    public String getKode() {
        // TODO: kembalikan kode AsistenDosen.
        return this.kode;
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        this.mahasiswa.add(mahasiswa);
        Collections.sort(this.mahasiswa);
        // TODO: tambahkan mahasiswa ke dalam daftar mahasiswa dengan mempertahankan urutan.
        // Hint: kamu boleh menggunakan Collections.sort atau melakukan sorting manual.
        // Note: manfaatkan method compareTo pada Mahasiswa.
    }

    public Mahasiswa getMahasiswa(String npm) {
        // TODO: kembalikan objek Mahasiswa dengan NPM tertentu dari daftar mahasiswa.
        // Note: jika tidak ada, kembalikan null atau lempar sebuah Exception.
        for (int i = 0; i < this.mahasiswa.size(); i++) {
            if (this.mahasiswa.get(i).getNpm().equals(npm)) {
                return this.mahasiswa.get(i);
            }
        }
        return null;
    }

    public String rekap() {
        String out = "";
        for (int i = 0; i < this.mahasiswa.size(); i++) {
            out += String.format("%s\n", this.mahasiswa.get(i).toString());
            out += String.format("%s\n", this.mahasiswa.get(i).rekap());
        }
        return out;
    }

    public String toString() {
        return String.format("%s - %s", this.kode, this.nama);
    }
}
